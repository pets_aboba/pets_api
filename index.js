const config = require('./config');
const express = require('express');
const crypto = require('crypto');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');
const client = new MongoClient(config.jdbc_url);

/**
 * Генерирует случайное число в указанном диапазоне.
 * @returns {number} Случайное число.
 */
const generateRandNumber = () => Math.floor(Math.random() * (9999999999 - 1000000000 + 1)) + 1000000000;

/**
 * Хеширует пароль с солью.
 * @param {string} password - Пароль.
 * @param {string} salt - Соль.
 * @returns {string} Захешированный пароль.
 */
function hashPassword(password, salt) {
    return crypto.createHash('sha256').update(password + salt).digest('hex');
}

/**
 * Получает коллекцию пользователей.
 * @returns {import('mongodb').Collection} Коллекция пользователей.
 */
const getUsersCollection = () => client.db('pets').collection('users');

/**
 * Получает коллекцию животных по категории.
 * @param {string} category - Категория животных.
 * @returns {import('mongodb').Collection} Коллекция животных.
 */
const getPetsCollection = (category) => client.db('pets').collection(`pets_cat_${Number(category)}`);

const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const PORT = 3000;

/**
 * Обработчик POST запроса для регистрации пользователей.
 * @param {import('express').Request} req - Объект запроса Express.
 * @param {import('express').Response} res - Объект ответа Express.
 * @returns {Promise<void>} Промис без возвращаемого значения.
 */
app.post('/api/register', async (req, res) => {
    try {
        const { name, email, password_sha256, salt } = req.body;
        const usersCollection = getUsersCollection();
        const user = await usersCollection.findOne({ email });

        if (user) {
            return res.status(400).json({
                status: false,
                error_code: `Юзер уже существует.`,
                user_id: null
            });
        }

        const user_id = generateRandNumber();

        await usersCollection.insertOne({
            email,
            name,
            user_id,
            salt,
            password: hashPassword(password_sha256, salt)
        });

        res.status(200).json({
            status: true,
            error_code: null,
            user_id
        });
    } catch (error) {
        console.error(error);
    }
});

/**
 * Обработчик POST запроса для входа пользователей.
 * @param {import('express').Request} req - Объект запроса Express.
 * @param {import('express').Response} res - Объект ответа Express.
 * @returns {Promise<void>} Промис без возвращаемого значения.
 */
app.post('/api/login', async (req, res) => {
    try {
        const { email, password_sha256 } = req.body;
        const usersCollection = getUsersCollection();
        const user = await usersCollection.findOne({ email });

        if (!user) {
            return res.status(401).json({
                status: false,
                error_code: `Юзера не существует.`,
            });
        }

        if (hashPassword(password_sha256, user.salt) !== user.password) {
            return res.status(401).json({
                status: false,
                error_code: `Пароль не верен.`,
            });
        }

        res.status(200).json({
            status: true,
            error_code: null,
        });
    } catch (error) {
        console.error(error);
    }
});

/**
 * Обработчик GET запроса для получения списка животных.
 * @param {import('express').Request} req - Объект запроса Express.
 * @param {import('express').Response} res - Объект ответа Express.
 * @returns {Promise<void>} Промис без возвращаемого значения.
 */
app.get('/api/get_pets', async (req, res) => {
    try {
        const { category, filter } = req.query;
        const { city_id, gender, age } = filter;

        const petsCollection = getPetsCollection(category);
        const pets = await petsCollection.find({ 
            city_id: Number(city_id), 
            gender: (gender == 'true'), 
            age: { $lt: Number(age[1]), $gt: Number(age[0]) } 
        }).toArray();
    
        if (!pets.length) {
            return res.status(401).json({
                status: false,
                error_code: `Ничего не найдено.`,
                animals_array: null
            });
        }
    
        res.status(200).json({
            status: true,
            error_code: null,
            animals_array: pets
        });
    } catch (error) {
        console.error(error);
    }
});

/**
 * Обработчик POST запроса для добавления новых животных.
 * @param {import('express').Request} req - Объект запроса Express.
 * @param {import('express').Response} res - Объект ответа Express.
 * @returns {Promise<void>} Промис без возвращаемого значения.
 */
app.post('/api/add_pets', async (req, res) => {
    try {
        const { img, animal_name, gender, age, category, city_id, descr, price } = req.body;
        const petsCollection = getPetsCollection(category);
        const adv_id = generateRandNumber();

        await petsCollection.insertOne({
            img,
            animal_name,
            gender,
            age,
            city_id,
            descr,
            price,
            adv_id
        });
    
        res.status(200).json({
            status: true,
            error_code: null
        });
    } catch (error) {
        console.error(error);
    }
});

/**
 * Устанавливает соединение с MongoDB и запускает сервер приложения.
 * @returns {Promise<void>} Промис без возвращаемого значения.
 */
client.connect().then(() => {
    console.log('Подключился к MongoDB');
}).catch(console.error);

/**
 * Начинает прослушивание порта и выводит сообщение о запуске сервера.
 * @returns {void} Функция без возвращаемого значения.
 */
app.listen(PORT, () => {
    console.log(`Сервер запущен на порту ${PORT}`);
});